% cerrar todas las ventanas y eliminar workspace
close all; clc; clear;

file_name = 'temp.wav';         
[x, t, Fs, dt, tf, TotalTime] = sample_audiofile(file_name,16);

% grfica de la seal muestreada
figure(1);
plot(t,x,'r');
title('Señal de sensor de temperatura 1°C/10mV');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)');                  

% ganancia de 1/100mV
figure(2);
x = x*(1/100e-3);
subplot(4,1,1);
plot(t,x,'r'); grid();
title('Señal de sensor de temperatura 1°C/100mV');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)');                  


lim_inf = 1.5;
lim_sup = 2.5;
y = window_detector(t,x, lim_inf, lim_sup);
subplot(4,1,2);
plot(t,y,'b');
title('Señal de salida del detector de ventana');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)');   

rst = square(2*pi*1*t, 10);
y = peak_detector(t, x, rst);
subplot(4,1,3)
plot(t,y,'black'); grid();
title('Señal de salida del detector de picos');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)'); 


subplot(4,1,4)
plot(t,rst,'black'); grid();
title('Señal de reinicio (rst) con un ciclo de trabajo de 0% @ 1Hz');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)'); 

rst = square(2*pi*1*t, 0);
y = peak_detector(t, x, rst);
figure(3)
plot(t,y,'red'); grid();
hold on;
plot(t,x,'black'); grid();
hold off;
title('Señal de salida del detector de picos vs señal amplificada');                     
xlabel('Tiempo (segundos)');                 
ylabel('Voltaje (V)'); 


function y = window_detector(t, x, lim_inf, lim_sup)
    fprintf("limite inferior = %i °C\n", lim_inf);
    fprintf("limite superior = %i °C\n", lim_sup);
    y = zeros(1,length(t));
    
    for i=1:length(t)
        if((x(i) > lim_inf) && (x(i) < lim_sup))
            y(i)=1;
        else
            y(i)=0;
        end
    end
end

function y = peak_detector(t, x, rst)
    y = zeros(1,length(t));
    x_max = 0;
    for i=1:length(t)
        if(1==rst(i))
            x_max = 0;
        end
        
        if(x(i) > x_max)
            x_max = x(i);
        end
        
        y(i) = x_max;
    end
end


function [xq,t,Fs, dt, tf, TotalTime] = sample_audiofile(file_name,k)
    [X, Fs] = audioread(file_name);
    X = X(:,1);
    
    % clculo del vector de tiempo
    TotalTime = length(X)./Fs;                    % duracin del audio
    dt = (TotalTime/(length(X)));                 % paso de tiempo
    tf = (TotalTime-(TotalTime/length(X)));       % tiempo final
    t = 0:dt:tf;                                  % definicin del v. tiempo
    
    % seal muestreada con sample-and-hold
    ts = 2;
    xs = zeros(1,length(t));
    
    for i=1:length(t)
    if(rem(i,ts)==1)
    tmp = X(i);
    end
    xs(i) = tmp;
    end

    % cuantificacin
    M = 2^k;

    int = (max(xs)-min(xs))/M;
    m = (min(xs)+int/2):int:(max(xs)-int/2);
    xq = zeros(1,length(t));
    
    for i=1:length(t)
        [tmp k] = min(abs(xs(i)-m));
        xq(i) = m(k);
    end
end

